## @file Lab1.py
#  Source Code: https://bitbucket.org/nlewis05/me305_labs/src/master/Lab%201/Lab1.py
def fib (idx):
    print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))
    if idx > 1:
        fib_num_last_last=0
        fib_num_last=1
        for x in range(idx-1):
            fib_num = fib_num_last_last + fib_num_last
            fib_num_last_last = fib_num_last
            fib_num_last=fib_num
        print(fib_num)
    elif idx == 1:
        fib_num=1
        print(fib_num)  
    elif idx == 0:
        fib_num=0
        print(fib_num)
    else:
        print("Error")
idx=input("Please enter a Fibonacci index greater than or equal to zero: ")
fib(int(idx))